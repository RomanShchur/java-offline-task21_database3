use storedpr_db;
DROP trigger IF EXISTS on_employee_identity_number_insert;
DELIMITER sEND
CREATE TRIGGER on_employee_identity_number_insert
BEFORE INSERT ON employee FOR EACH ROW
BEGIN
  IF new.identity_number LIKE '%00' THEN
      SIGNAL sqlstate '45000' set message_text = "No way! Identity number cannot end with 00!";
  END IF;
END;
sEND
DELIMITER ;

use storedptr_db;
DROP trigger IF EXISTS on_employeee_identity_number_update;
DELIMITER sEND
CREATE TRIGGER on_employeee_identity_number_update
BEFORE UPDATE
ON employee FOR EACH ROW
BEGIN
  IF new.identity_number LIKE '%00' THEN
      SIGNAL sqlstate '45000' set message_text = "No way! Identity number cannot end with 00!";
  END IF;
END;
sEND
DELIMITER ;

use storedptr_db;
DROP trigger IF EXISTS on_medicine_ministry_code_insert;
DELIMITER sEND
CREATE TRIGGER on_medicine_ministry_code_insert
BEFORE INSERT
ON medicine FOR EACH ROW
BEGIN
  IF new.ministry_code NOT RLIKE '[[а-л][н-о][р-я]][[а-л][н-о][р-я]]-[1-9][1-9][1-9]-[1-9][1-9]' THEN
      SIGNAL sqlstate '45000' set message_text = "No way! Ministry code wrong!";
  END IF;
END;
sEND
DELIMITER ;

use storedptr_db;
DROP trigger IF EXISTS on_medicine_ministry_code_update;
DELIMITER sEND
CREATE TRIGGER on_medicine_ministry_code_update
BEFORE UPDATE
ON medicine FOR EACH ROW
BEGIN
  IF new.ministry_code NOT RLIKE '[[а-л][н-о][р-я]][[а-л][н-о][р-я]]-[1-9][1-9][1-9]-[1-9][1-9]' THEN
      SIGNAL sqlstate '45000' set message_text = "No way! Ministry code wrong!";
  END IF;
END;
sEND
DELIMITER ;

use storedptr_db;
DROP trigger IF EXISTS on_post_insert;
DELIMITER sEND
CREATE TRIGGER on_post_insert
BEFORE INSERT
ON post FOR EACH ROW
BEGIN
  SIGNAL sqlstate '45000' set message_text = "No way! No post insert!";
END;
sEND
DELIMITER ;
DELIMITER sEND

use storedptr_db;
DROP trigger IF EXISTS on_post_update;
CREATE TRIGGER on_post_update
BEFORE UPDATE
ON post FOR EACH ROW
BEGIN
  SIGNAL sqlstate '45000' set message_text = "No way! No post update!";
END;
sEND
DELIMITER ;

use storedptr_db;
DROP trigger IF EXISTS on_post_delete;
DELIMITER sEND
CREATE TRIGGER on_post_delete
BEFORE DELETE
ON post FOR EACH ROW
BEGIN
  SIGNAL sqlstate '45000' set message_text = "No way! No post delete";
END;
sEND
DELIMITER ;
