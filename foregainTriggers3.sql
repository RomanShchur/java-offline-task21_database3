use storedptr_db;
DROP PROCEDURE IF EXISTS find_min_exp;
DELIMITER sEND
CREATE PROCEDURE find_min_exp ()
BEGIN
  SELECT surname, name, experience FROM employee
  WHERE MIN(experience);
END;
sEND
DELIMITER ;

use storedptr_db;
DROP PROCEDURE IF EXISTS SP_CREATE_TABLE_TEST;
DELIMITER sEND
CREATE PROCEDURE SP_CREATE_TABLE_TEST ()
BEGIN
  DECLARE i int;
  SET i = 5;
  label1: LOOP
    IF i < 0 THEN LEAVE label1;
    END IF;
    SET @statement = CONCAT('
      CREATE TABLE employee',i,' (
        TestID int(11) default NULL,
        TestName varchar(100) default NULL
      );'
    );
    PREPARE stmt FROM @statement;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
    SET i = i - 1;
    ITERATE label1;
  END LOOP;
END;
sEND
DELIMITER ;
