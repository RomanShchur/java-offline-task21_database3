use storedptr_db;
DROP trigger IF EXISTS on_post_delete;
DELIMITER sEND
CREATE TRIGGER on_post_delete
BEFORE DELETE
ON post FOR EACH ROW
BEGIN
  DECLARE old_post VARCHAR(15);
  DECLARE busy_post VARCHAR(15);
  SELECT post INTO old_post FROM post WHERE post = OLD.post;
  SELECT post INTO busy_post FROM employee WHERE post = old_post;
  IF busy_post = old_post THEN
      SIGNAL sqlstate '45000' set message_text = "No way! You cannot delete foregain inherited key!";
  END IF;
END;
sEND
DELIMITER ;

use storedptr_db;
DROP trigger IF EXISTS on_post_update;
DELIMITER sEND
CREATE TRIGGER on_post_update
BEFORE UPDATE
ON post FOR EACH ROW
BEGIN
  DECLARE old_post VARCHAR(15);
  DECLARE busy_post VARCHAR(15);
  SELECT post INTO old_post FROM post WHERE post = OLD.post;
  SELECT post INTO busy_post FROM employee WHERE post = old_post;
  IF busy_post = old_post THEN
      SIGNAL sqlstate '45000' set message_text = "No way! You cannot update foregain inherited key!";
  END IF;
END;
sEND
DELIMITER ;

use storedptr_db;
DROP trigger IF EXISTS on_employeee_insert;
DELIMITER sEND
CREATE TRIGGER on_employeee_insert
BEFORE INSERT
ON employee FOR EACH ROW
BEGIN
  DECLARE old_post VARCHAR(15);
  DECLARE new_post VARCHAR(15);
  SELECT post INTO old_post FROM post WHERE post = NEW.post;
  SELECT post INTO new_post FROM employee WHERE post = old_post;
  IF new_post = old_post THEN
      SIGNAL sqlstate '45000' set message_text = "No way! You cannot inset nonexistent foregain key!";
  END IF;
END;
sEND
DELIMITER ;

use storedptr_db;
DROP trigger IF EXISTS on_employeee_update;
DELIMITER sEND
CREATE TRIGGER on_employeee_update
BEFORE UPDATE
ON employee FOR EACH ROW
BEGIN
  DECLARE old_post VARCHAR(15);
  DECLARE new_post VARCHAR(15);
  SELECT post INTO old_post FROM post WHERE post = NEW.post;
  SELECT post INTO new_post FROM employee WHERE post = old_post;
  IF new_post = old_post THEN
      SIGNAL sqlstate '45000' set message_text = "No way! You cannot update to nonexistent foregain key!";
  END IF;
END;
sEND
DELIMITER ;
